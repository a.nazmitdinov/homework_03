package src;

import java.util.Scanner;

public class Test {
    public static void main(String[] args){
        System.out.println("\nЗадание №1 ");
        for (int i = 1; i < 5; i++) {
            int j = 0;
            while (j < i) {
                System.out.print(j + " ");
                j++;
            }
            System.out.print("\n");
        }
        System.out.println("\nЗадание №2 ");
        int x = 0;
        while  (x < 5) {
            for (int j = x; j > 1; j--)
                System.out.print(j + " ");
            System.out.println("****");
            x++;
        }
        System.out.println("\nЗадание №3 ");
        int i = 5;
        while (i >= 1) {
            int num = 1;
            for (int j = 1; j <= i; j++) {
                System.out.print(num + "xxx");
                num *= 2;
            }
            System.out.println();
            i--;
        }
        System.out.println("\nЗадание №4 ");
        int k = 1;
        do {
            int num = 1;
            for (int j = 1; j <= k; j++) {
                System.out.print(num + "G");
                num += 2;
            }
            System.out.println();
            k++;
        } while (k <= 5);
    }
}