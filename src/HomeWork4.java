package src;

import java.util.Scanner;

public class HomeWork4 {
    /*
    Дано натуральное число n. Вывести его цифры в “столбик”.
Ограничения:
0 < n < 1000000
Пример:
     Входные данные        Выходные данные
          74                      7
                                  4

        1630                      1
                                  6
                                  3
                                  0
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String number = input.nextLine();

        int n = number.length();

        for (int i = 0; i < n; i++) {
            System.out.println(number.charAt(i));
        }
    }
}
