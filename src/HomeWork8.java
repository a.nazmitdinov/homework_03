package src;

import java.util.Scanner;
/*
На вход подается:
                  ○ целое число n,
                  ○ целое число p
                  ○ целые числа a1, a2 , … an
Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго
больше p.
Ограничения:
0 < m, n, ai < 1000
Пример:
             Входные данные               Выходные данные
                   2
                   18                           126
                   95 31
            ______________________________________________
                   6
                   29                           326
             40 37 97 72 80 18
            ______________________________________________
                   1
                   100                          0
                   42
 */
public class HomeWork8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Введите число n ");
        int n = input.nextInt();
        System.out.println("Введитте число p ");
        int p = input.nextInt();
        System.out.println("Введите оставшиеся значения ");
        input.nextLine();
        String text = input.nextLine();

        char space = ' ';
        int length = text.length(); // длина строки
        int firstNumber; // начальное значение для извлечения подстроки
        int endNumber  = length; // конечное значение для извлечения подстроки
        int sum1 = 0;
        int sum2 = 0;

        // Извлечение чисел начиная с последнего
        // до второго числа из введенного текста

        for (int k = length-1; k >= 0; k--) {  // цикл для извлечения чисел из текста (кроме первого значения)
            if (text.charAt(k) == space) {
                firstNumber = k;
                String number = text.substring(firstNumber + 1, endNumber);
                int digit = Integer.parseInt(number); // преобразование полученного значения в число
                if (digit > p) {          // проверка, что полученное значение больше, чем p
                    sum1 = sum1 + digit;  // находим сумму чисел, которые больше, чем p
                }
                endNumber = firstNumber; // сокращаем длину строки
            }
        }

        // Вывод первого числа из введенного текста

        if (text.indexOf(space) >= 0) { // проверяем есть ли пробел в текстовм значении
            String number1 = text.substring(0, text.indexOf(space));
            int digit = Integer.parseInt(number1);
            if (digit > p) {               // проверка, что полученное значение больше, чем p
                sum2 = digit;
            }
        }
        else if (text.indexOf(space) == -1) {
            String number2 = text.substring(0, text.length());
            int digit = Integer.parseInt(number2);
            if (digit > p) {
                sum2 = digit;
            }
        }
            System.out.println(sum1 + sum2); // складываем все полученные суммы чисел
    }
}

