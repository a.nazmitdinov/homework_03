package src;
/*
                             ПРОЕКТ. Часть 3
 */

import java.util.Scanner;

public class ConvertAmount_03 {
    public static void main(String[] args) {
        final double ROUBLES_PER_CZK = 2.58; // курс покупки чешских крон
        int czk; // сумма денег в чешских кронах (CZK)
        int n; // количество конвертаций
        double roubles; // сумма денег в российских рублях
        int digit; // последняя цифра в CZK
        int i; // счетчик

        Scanner input = new Scanner(System.in);

        // Получать количество конвертаций до тех пор,
        // пока не введено корректное значение
        do {
            System.out.println("Введите корректное количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);

        // До тех пор, пока не конвертированы все суммы, получать,
        // отображать и конвертировать суммы денег в чешских кронах
        // и отображать сумму денег в российских рублях.
        for (i = 0; i < n; ++i) {

            // Получить сумму денег в чешских кронах
            System.out.print("Введите сумму в чешских кронах: ");
            czk = input.nextInt();

            // Отобразить сумму денег в чешских кронах
            // с правильным окончанием
            System.out.print(czk);

            if (5 <= czk && czk <= 20)
                System.out.println(" чешских крон равны ");
            else {
                digit = czk % 10;

                if (digit == 1)
                    System.out.println(" чешская крона равна ");
                else if (2 <= digit && digit <= 4)
                    System.out.println(" чешские кроны равны ");
                else
                    System.out.println(" чешских крон равны ");
            }
            // Конвертировать сумму денег в российские рубли
            roubles = ROUBLES_PER_CZK * czk;

            // Отобразить сумму денег в российских рублях в пользу покупателя
            System.out.println("Результат конвертирования: " + (int) (roubles * 100) / 100.0 + " руб.");
        }
    }
}
