package src;

import java.util.Scanner;

public class HomeWork9 {
    public static void main(String[] args) {
/*
На вход последовательно подается возрастающая последовательность из n
целых чисел, которая может начинаться с отрицательного числа.
Посчитать и вывести на экран, какое количество отрицательных чисел было
введено в начале последовательности. Помимо этого нужно прекратить
выполнение цикла при получении первого неотрицательного числа на вход.
Ограничения:
0 < n < 1000
-1000 < ai < 1000
   Пример:
      Входные данные                Выходные данные
   -55 -42 -19 -15 17 33                   4
   -------------------------------------------------
    10 20                                  0
   -------------------------------------------------
 */
        Scanner input = new Scanner(System.in);
        String text = input.nextLine();

        int sum = 0;

        String[] arr = text.split(" ");

        for (String str : arr) {
            int a = Integer.parseInt(str);
            if (a < 0) {
                sum = sum + 1;
            }
            else if (a >= 0) {
                break;
            }
        }
        System.out.println(sum);
    }
}
