package src;

import java.util.Scanner;

public class HomeWork6 {
    /*
    В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
число n - количество денег для размена. Необходимо найти минимальное
количество купюр с помощью которых можно разменять это количество денег
(соблюсти порядок: первым числом вывести количество купюр номиналом 8,
вторым - 4 и т д)
Ограничения:
0 < n < 1000000
Пример:
     Входные данные             Выходные данные
          51                        6 0 1 1
          10                        1 0 1 0
          60                        7 1 0 0
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int money = input.nextInt();

            int number1 = money / 8;
            money = money % 8;

            int number2= money / 4;
            money = money % 4;

            int number3 = money / 2;
            money = money % 2;

            int number4 = money / 1;

        System.out.println(number1 + " " + number2 + " " + number3 + " " + number4);
    }
}


