package src;

import java.util.Scanner;
/*
На вход подается два положительных числа m и n.
Необходимо вычислить m^1 + m^2 + ... + m^n
Ограничения:
0 < m, n < 10
Пример:
    Входные данные              Выходные данные
          1 1                         1
          8 5                       37448
 */

public class HomeWork3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int m = input.nextInt();
        int n = input.nextInt();
        double sum = 0;

        for (int i = 1; i <= n; i++) {
            sum = sum + Math.pow(m, i);
        }
        System.out.println((int)sum);
    }
}
