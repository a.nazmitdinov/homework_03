package src;

public class HomeWork1 {
 /*
    Напечатать таблицу умножения от 1 до 9. Входных данных нет. Многоточие в
примере ниже подразумевает вывод таблицы умножения и для остальных чисел
2, 3 и т. д.
             1 x 1 = 1
             1 x 2 = 2
             .........
             9 x 8 = 72
             9 x 9 = 81
     */

    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= 9; j++)
                System.out.println(i + " x " + j + " = " + i * j);
        }
    }
}
