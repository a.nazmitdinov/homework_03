package src;

public class PaintWindow {
    public static void main(String[] args) {
        int BLOCK_SIZE = 4;
        for (int i=1; i<BLOCK_SIZE; i++){
            for(int j=1; j<BLOCK_SIZE; j++){
                System.out.print("+===");
            }
            System.out.println("+");
            for(int k=0; k<3; k++){
                for(int j=1; j<BLOCK_SIZE; j++){
                    System.out.print("|   ");
                }
                System.out.println("|");
            }
        }
        for(int j=1; j<BLOCK_SIZE; j++){
            System.out.print("+===");
        }
        System.out.println("+");
    }
}
