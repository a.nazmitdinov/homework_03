package src;
import java.util.Scanner;
/*
Вывести на экран “ёлочку” из символа звездочки (*) заданной высоты N. На N +
1 строке у “ёлочки” должен быть отображен ствол из символа |
Ограничения:
2 < n < 10
Пример:
       Входные данные            Выходные данные
                                        #
             3                         ###
                                      #####
                                        |
      -------------------------------------------
                                        #
                                       ###
                                      #####
             6                       #######
                                    #########
                                   ###########
                                        |
      ------------------------------------------
 */
public class HomeWork10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите размер ёлочки: ");
        int size = input.nextInt();

        for (int i = 0; i < size; i++) { // добавление строк
            for (int j = 0; j < i * 2 + 1; j++) { // создание столбцов
                if (i == 0) { // цикл по созданию вершины елочки
                    for (int k = 0; k < (size - 1); k++) {
                        System.out.print("O"); // поставил "O" вместо пробела для наглядности
                    }
                    System.out.print("#");
                } else {
                    System.out.print("#");
                }
            }
            System.out.println("A"); // переход на новую строчку, поставил "A" вместо пробела для наглядности
            for (int k = i; k < (size - 2); ++k) { // centering CT
                System.out.print("O"); // поставил "O" вместо пробела для наглядности
            }
        }
        // Вывод на печать ствола ёлки
        for (int trunk = 0; trunk < (size - 1); trunk++) {
            System.out.print("O"); // поставил "O" вместо пробела для наглядности
        }
        System.out.print("|");
    }
}