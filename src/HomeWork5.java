package src;

import java.util.Scanner;

public class HomeWork5 {
    /*
    Даны положительные натуральные числа m и n. Найти остаток от деления m на
n, не выполняя операцию взятия остатка.
Ограничения:
0 < m, n < 10
Пример:
Java 12 Базовый модуль Неделя 2
ДЗ 1 Часть 2
Входные данные                 Выходные данные
     9 1                              0
     8 3                              2
     7 9                              7

     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int m = input.nextInt();
        int n= input.nextInt();

        double division = m/n;

        int remainder = m - n * (int)division;

        System.out.println(remainder);
    }
}
