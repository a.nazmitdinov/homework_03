package src;

import java.util.Scanner;

public class HomeWork7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String text = input.nextLine();

        int sum = 0;
        int number = 1;
        char ch;

        for (int i = 0; i < text.length() - 1; i++) {
            ch = text.charAt(i);
            if (ch != ' ') {
                number ++;
                continue;
            }
        }
        System.out.println(number);
    }
}
