package src;

import java.util.Scanner;
/*
На вход подается два положительных числа m и n. Найти сумму чисел между m
и n включительно.
Ограничения:
0 < m, n < 10
m < n
Пример:
Входные данные             Выходные данные
     7 9                         24
     1 2                         3
 */

public class HomeWork2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int m = input.nextInt();
        int n = input.nextInt();
        int sum = 0;

        for (int i = m; i <= n; i++) {
            sum += i;
        }
        System.out.println(sum);
    }
}
